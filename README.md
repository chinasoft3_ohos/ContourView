# ContourView

#### 项目介绍
- 项目名称：`ContourView`自定义 View：用贝塞尔曲线绘制酷炫轮廓背景
- 所属系列：openharmony的第三方组件适配移植
- 功能：自定义 View：用贝塞尔曲线绘制酷炫轮廓背景
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 1.0.1

#### 效果演示
![screen1](./image/ContourView.gif "屏幕截图")

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
             url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在app模块的build.gradle文件中，

 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:contourView:1.0.0')
    ......  
 }
 ```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
使用该库非常简单，只需查看提供的示例的源代码。
创建视图*ContourView*
```
 <com.ocnyang.contourview.ContourView
            ohos:height="match_parent"
            ohos:width="match_parent"
            app:contour_style="Sand"
            app:shader_mode="Radial"
            app:shader_startcolor="$color:startcolor"
            app:shader_endcolor="$color:endcolor"
            app:shader_style="Left_To_Bottom"
            />
```
 java 中使用
```
private void initCustomContourView() {
        ContourView contourViewCustom = (ContourView) findComponentById(ResourceTable.Id_contourview_custom);
        int width = getWidth();
        int hight = 700;
        int[] ints = {width / 2, 50, ((int) (width * 0.75)), hight / 2, ((int) (width * 0.35)), 350};
        int[] intArr = new int[]{width / 5, hight / 3, width / 4 * 3, hight / 2, width / 2, ((int) (hight * 0.9)), width / 5, ((int) (hight * 0.8))};
        contourViewCustom.setPoints(ints, intArr);
        try {
            contourViewCustom.setShaderStartColor(new Color(getResourceManager().getElement(ResourceTable.Color_startcolor).getColor()));
            contourViewCustom.setShaderEndColor(new Color(getResourceManager().getElement(ResourceTable.Color_endcolor).getColor()));
        } catch (Exception e) {
        }
        contourViewCustom.setShaderMode(ContourView.SHADER_MODE_RADIAL);
    }
```


 属性设置

| （xml）属性名称 | 说明 | 值类型 |
|:----:|:----|:----|
| contour_style | 内置轮廓样式 | Beach，Ripples，Clouds，Sand，Shell |
| smoothness | 轮廓弯曲系数（没有必要的情况下，不建议设置） | Float类型 范围：0--1，建议范围：0.15--0.3，默认：0.25 |
| shader_mode | 轮廓内颜色的填充方式 | Radial，Sweep，Linear，不设置默认纯色填充 |
| shader_startcolor | 填充起始颜色 | color类型，半透明效果，设置类似#90FF0000的值（默认白色，需设置shader_mode 才有效果） |
| shader_endcolor | 填充结束颜色 | 同上 |
| shader_style | 填充起始点及方向的控制 | LeftToBottom（左上角到右下角），RightToBottom（右上角到左下角），TopToBottom（上中点到下中点），Center（中点到右下角） |
| shader_color | 填充纯色颜色 | color类型，默认白色，不设置shader_mode时，可以通过此属性设置纯色填充颜色 |

 动态属性

以上的（xml）属性都有对应的设置方法。
此外，还有一些可以动态设置的属性。

**轮廓锚点坐标集**

	public void setPoints(List<Point[]> pointsList)
	//List<> 表示轮廓的个数，Point[] 表示具体某个轮廓的坐标集，每个轮廓至少4个锚点。

| 方法参数 | 说明 |
|---------|------|
| setPoints(int... pts) | 单个轮廓，int[]{锚点1.x，锚点1.y，锚点2.x，锚点2.y......锚点n.x，锚点y} |
| setPoints(Point[]... pointsArr) | 单个轮廓 |
| setPoints(Point... points) | 多个轮廓 |
| setPoints(int[]... ptsArr) | 多个轮廓 |

**Shader 自定义轮廓绘制方式**

	public void setShader(Shader... shader)
	//Shader：传入自己自定义RadialGradient，SweepGradient，LinearGradient。
	//当传入多个Shader时,给多个轮廓设置不同的绘制方式，Shader[0]填充轮廓1，Shader[1]填充轮廓2...






 Step2. 在布局文件中使用，也可以设置相应的自定义属性

	<com.ocnyang.contourview.ContourView
        ohos:layout_width="match_parent"
        ohos:layout_height="400dp"
        app:contour_style="Ripples"
        app:shader_endcolor="@color/endcolor"
        app:shader_mode="RadialGradient"
        app:shader_startcolor="@color/startcolor"
        app:shader_style="Center"
        app:smoothness="0.2"/>

根据自己的需要来设置属性。

 Step3. 如果需要自定义自己独特的轮廓，可以在代码中动态设置以下内容

    /**
     * Customize the coordinates of the anchor to control the area to be drawn。
     */
    private void initCustomContourView() {
        ContourView contourViewCustom = (ContourView) findViewById(R.id.contourview_custom);
        int width = getWidth();//获取屏幕的宽度
        int hight = 400;
        int[] ints = {width / 2, 0, width, hight / 2, width / 2, hight, 0, hight / 2};
        int[] intArr = new int[]{width / 2, hight / 4, width / 4 * 3, hight / 2, width / 2, hight / 4 * 3, width / 4, hight / 2};
        contourViewCustom.setPoints(ints, intArr);
        contourViewCustom.setShaderStartColor(getResources().getColor(R.color.startcolor));
        contourViewCustom.setShaderEndColor(getResources().getColor(R.color.endcolor));
        contourViewCustom.setShaderMode(ContourView.SHADER_MODE_RADIAL);
    }

    /**
     * Controls the color of the drawing.
     */
    private void initBeachContourView() {
        ContourView contourViewBeach = ((ContourView) findViewById(R.id.contourview_beach));

        RadialGradient radialGradient = new RadialGradient(0, 0,4000,
                getResources().getColor(R.color.startcolor),
                getResources().getColor(R.color.endcolor),
                Shader.TileMode.CLAMP);
        LinearGradient linearGradient = new LinearGradient(0, 0, getWidth(), 400,
                Color.argb(30, 255, 255, 255), Color.argb(90, 255, 255, 255),
                Shader.TileMode.REPEAT);
        contourViewBeach.setShader(radialGradient, linearGradient);
    }

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 0.0.1-SNAPSHOT

#### 版权和许可信息

Apache 2.0 License

