/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ocnyang.contourviewdemo;

import com.ocnyang.contourview.PointsFactory;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.Point;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * 单元测试
 */
public class MainAbilityTest {
    /**
     * onStart
     */
    @Test
    public void onStart() {
    }

    /**
     * testBundleName
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.ocnyang.contourviewdemo", actualBundleName);
    }

    /**
     * testSandPoints
     */
    @Test
    public void testSandPoints() {
        List<Point[]> styleSandPoints = PointsFactory.getStyleSandPoints(300, 400);
        assertEquals(2, styleSandPoints.size());
        assertEquals(4, styleSandPoints.get(0).length);
        assertEquals(4, styleSandPoints.get(1).length);
    }

    /**
     * testBeachPoints
     */
    @Test
    public void testBeachPoints() {
        testHeightBig();
        testWidthBig();
    }

    /**
     * testHeightBig
     */
    private void testHeightBig() {
        int width = 300;
        int height = 400;
        List<Point[]> styleBeachPoints = PointsFactory.getStyleBeachPoints(width, height);
        assertEquals(2, styleBeachPoints.size());
        Point[] points = styleBeachPoints.get(1);
        Point[] points1 = styleBeachPoints.get(0);
        int distance = (int) (width * 0.05);
        for (int i = 0; i < points.length; i++) {
            if (i == points.length - 2 || i == points.length - 1) {
                assertEquals(points[i].getPointXToInt(), points1[i].getPointXToInt());
                assertEquals(points[i].getPointYToInt(), points1[i].getPointYToInt());
            } else {
                assertEquals(points1[i].getPointXToInt(), points[i].getPointXToInt() + (i == 0 ? 0 : distance));
                assertEquals(points1[i].getPointYToInt(), points[i].getPointYToInt() + distance);
            }
        }
    }

    /**
     * testWidthBig
     */
    private void testWidthBig() {
        int width = 400;
        int height = 300;
        int distance = (int) (width * 0.05);
        List<Point[]> styleBeachPoints = PointsFactory.getStyleBeachPoints(width, height);
        assertEquals(2, styleBeachPoints.size());
        Point[] points = styleBeachPoints.get(0);
        Point[] points1 = styleBeachPoints.get(1);
        for (int i = 0; i < points.length; i++) {
            if (i == 0 || i == points.length - 2) {
                assertEquals(points1[i].getPointXToInt(), points[i].getPointXToInt() + (i == 0 ? distance : 0));
                assertEquals(points1[i].getPointYToInt(), points[i].getPointYToInt() - (i == points.length - 2 ? distance : 0));
            } else {
                assertEquals(points1[i].getPointXToInt(), points[i].getPointXToInt() + distance);
                assertEquals(points1[i].getPointYToInt(), points[i].getPointYToInt() - distance);
            }
        }
    }

    /**
     * testCloudsPoint
     */
    @Test
    public void testCloudsPoint() {
        int width = 300;
        int height = 400;
        List<Point[]> styleCloudsPoints = PointsFactory.getStyleCloudsPoints(width, height);
        assertEquals(3, styleCloudsPoints.size());
        assertEquals(4, styleCloudsPoints.get(0).length);
        assertEquals(4, styleCloudsPoints.get(1).length);

    }
}