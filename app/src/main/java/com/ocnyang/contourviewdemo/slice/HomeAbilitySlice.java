/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.ocnyang.contourviewdemo.slice;

import com.ocnyang.contourviewdemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Image;
import ohos.agp.components.element.ShapeElement;

import ohos.utils.net.Uri;

/**
 * @Description: 引导页
 * @Author: yizhihao
 * @CreateDate: 2021/5/27 10:39
 */
public class HomeAbilitySlice extends AbilitySlice {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_home);

        Image image = (Image) findComponentById(ResourceTable.Id_image_directional_layout);

        image.setClickedListener(component -> {
            Uri uri = Uri.parse("https://github.com/OCNYang/ContourView");

            Intent intent1 = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withUri(uri)
                    .build();
            intent1.setOperation(operation);
            startAbility(intent1);
        });

        DependentLayout dependTop = (DependentLayout) findComponentById(ResourceTable.Id_depend_top);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(ShapeElement.RECTANGLE);
        shapeElement.setShaderType(ShapeElement.LINEAR_GRADIENT_SHADER_TYPE);
        RgbColor[] rgbColors = new RgbColor[]{new RgbColor(228, 82, 251),
                new RgbColor(161, 66, 254),
                new RgbColor(120, 58, 255)};
        shapeElement.setRgbColors(rgbColors);
        dependTop.setBackground(shapeElement);
        ShapeElement shapeElement1 = new ShapeElement();
        shapeElement1.setShaderType(ShapeElement.RECTANGLE);
        shapeElement1.setShaderType(ShapeElement.LINEAR_GRADIENT_SHADER_TYPE);
        shapeElement1.setRgbColors(rgbColors);
        shapeElement1.setCornerRadius(50);
        shapeElement1.setGradientOrientation(ShapeElement.Orientation.LEFT_TO_RIGHT);
        Button buttonNext = (Button) findComponentById(ResourceTable.Id_next);
        buttonNext.setBackground(shapeElement1);
        buttonNext.setClickedListener(component -> present(new MainAbilitySlice(), new Intent()));
    }

}
